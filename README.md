# arename
![](https://lacave.stream/img/arename.jpg)

Utilitaire de renommage automatisé de fichiers vidéos en masse, conçu pour [LaCave](https://lacave.stream)

Merci à Erwan L. pour le script initial.

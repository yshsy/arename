#!/bin/bash
figlet arename
printf "\nATTENTION ! Ne pas mettre différents contenus dans le même dossier\n\n"
printf "Sélectionner le type de contenu :\n [1] Série / Anime\n [2] Film\n\n"
read -p "Choix : " TYPE
printf '%s\n'
FILES=$(find . -maxdepth 1 -type f \( -name "*.mkv" -o -name "*.mp4" -o -name "*.avi" \) | sort -V)

# Série / Anime
if [ $TYPE == '1' ]; then
  read -p "Entrez le nom de la série / anime : " NAME
  read -p "Entrez le numéro de la saison : " SAISON
  NAME=${NAME// /_}; ep=0

  while IFS= read f; do
    EXTENSION=$(echo "$f" | sed 's/^.*\.//')
    ep=$(($ep+1))

    if [ $SAISON -lt 10 ]; then S=S0; else S=S; fi
    if [ $ep -lt 10 ]; then EPI='E0'; else EPI='E'; fi

    rename="${NAME}-${S}${SAISON}${EPI}${ep}.${EXTENSION}"
    mv "$f" "$rename"
  done <<< "$FILES"

# Film
elif [ $TYPE == '2' ]; then
  while IFS= read f; do
    EXTENSION=$(echo "$f" | sed 's/^.*\.//')

    printf "\nNom du fichier : $f\n"
    read -p "Nouveau nom : " NAME </dev/tty
    read -p "Année de sortie du film : " ANNEE </dev/tty
    NAME=${NAME// /_}

    rename="${NAME}-${ANNEE}.${EXTENSION}"
    mv "$f" "$rename"
  done <<< "$FILES"
fi
